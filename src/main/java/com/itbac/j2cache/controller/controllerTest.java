package com.itbac.j2cache.controller;

import com.github.benmanes.caffeine.cache.Cache;
import com.itbac.j2cache.client.J2CacheClient;
import com.itbac.j2cache.repository.KvRepository;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.concurrent.ConcurrentMap;


/**
 * @description： 测试用入口
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
@Controller
@RequestMapping()
@ResponseBody
public class controllerTest {

    @Autowired
    private J2CacheClient j2CacheClient;
    @Resource(name = "caffeinecache")
    private Cache cache;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private KvRepository kvRepository;


    @RequestMapping("/get/{key}")
    public String get(@PathVariable("key")  String key){
        String value = j2CacheClient.getValue(key);
        if (StringUtils.isEmpty(value)) {
            return "空";
        }
        return value;
    }
    @RequestMapping("/getByRepository/{key}")
    public String getByRepository(@PathVariable("key")  String key){
        //测试 J2Cacheable 自定义注解是否起作用
        String value = kvRepository.get(key);
        if (StringUtils.isEmpty(value)) {
            return "空";
        }
        return value;
    }


    //测试redis 是否连通
    @RequestMapping("/getByRedis/{key}")
    public String getByRedis(@PathVariable("key")  String key){
        Object value = redisTemplate.opsForValue().get(key);
        if (StringUtils.isEmpty(value)) {
            return "空";
        }
        return String.valueOf(value);
    }

    @RequestMapping("/del/{key}")
    public boolean delKey(@PathVariable("key")  String key){
        j2CacheClient.delKey(key, true);
        return true;
    }
    @RequestMapping("/getLocalCache")
    public String getLocalCache(){
        @NonNull ConcurrentMap concurrentMap = cache.asMap();
        if (concurrentMap.isEmpty()) {
            return "空";
        }
        return concurrentMap.toString();
    }

}
