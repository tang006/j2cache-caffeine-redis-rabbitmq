package com.itbac.j2cache.rabbitmq.constants;

/**
 * 需要通知删除本地缓存的队列名称
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/3/7 12:42
 */
public interface LocalCacheNoticeQueueName {
    String getName();
}
