package com.itbac.j2cache.rabbitmq.message;

import java.util.Arrays;

/**
 * @description 缓存清除消息通知包装类
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
public class J2CacheNotice {
    //每个节点，一个通道
    private String queueName;
    //操作：0，删除指定key缓存， 1 清空所有本地缓存 3，加入集群 ，4 退出集群。
    private int operator;
    //缓存key
    private String[] keys;
    //消息描述
    private String msg;

    public  boolean isLocalNotice(String queueName){
        return this.queueName.equals(queueName);
    }


    public String getQueueName() {
        return queueName;
    }

    public J2CacheNotice setQueueName(String queueName) {
        this.queueName = queueName;
        return this;
    }

    public int getOperator() {
        return operator;
    }

    public J2CacheNotice setOperator(int operator) {
        this.operator = operator;
        return this;
    }

    public String[] getKeys() {
        return keys;
    }

    public J2CacheNotice setKeys(String[] keys) {
        this.keys = keys;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public J2CacheNotice setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    @Override
    public String toString() {
        return "J2CacheNotice{" +
                "queueName='" + queueName + '\'' +
                ", operator=" + operator +
                ", keys=" + Arrays.toString(keys) +
                ", msg='" + msg + '\'' +
                '}';
    }
}
