package com.itbac.j2cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @description 启动类...
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.itbac.j2cache")
public class SpringBootJ2CacheApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringBootJ2CacheApplication.class, args);

    }

}
