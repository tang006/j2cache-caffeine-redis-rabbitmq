package com.itbac.j2cache.repository;

import com.itbac.j2cache.aop.annotatin.J2CacheEvict;
import com.itbac.j2cache.aop.annotatin.J2CachePut;
import com.itbac.j2cache.aop.annotatin.J2Cacheable;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description 自定义仓库，用于测试
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
@Repository
public class KvRepository {

    private  Map<String, String> map = new ConcurrentHashMap<>();

    public Map<String, String> getMap() {
        return map;
    }

    public KvRepository setMap(Map<String, String> map) {
        map = map;
        return this;
    }
    @J2Cacheable(cacheKey = "#key")
    public String get(String key){
        return map.get(key);
    }
    @J2CachePut(cacheKey = "#key")
    public String update(String key ,String value){
        map.put(key, value);
        return value;
    }
    @J2CacheEvict(cacheKey = "#key")
    public void del(String key){
        map.remove(key);
    }

    @PostConstruct
    public void  init(){
        //初始化仓库数据
        this.map.put("1", "1");
        this.map.put("2", "2");
        this.map.put("3", "3");
        this.map.put("4", "4");
        this.map.put("5", "5");
    }
}
