# j2cache-caffeine-redis-rabbitmq

#### 介绍
项目名有点长，但是可以让你秒懂。使用caffeine本地缓存、Redis分布式缓存、rabbitmq消息中间件的组合，实现二级缓存框架。

#### 使用说明

1.  J2CacheClient 接口，封装了caffeine本地缓存、Redis分布式缓存的操作。
2.  J2Cacheable，J2CacheEvict，J2CachePut 三个注解，通过AOP简化了缓存的使用，参考Spring Cache功能。
3.  J2CacheNoticeProducer 生成消息，J2CacheNoticeConsumer消费消息，实现通知本地缓存节点的删除缓存功能。

#### 参考

1.  本项目参考开源项目J2Cache，分析源码后的实践，开源出来，希望大家一起学习，交流，进步。
2.  自定义注解，参考Spring Cache功能。
